extern crate chrono;
use schema::document;
use self::chrono::NaiveDateTime;

#[derive(Serialize, Deserialize, Debug, Queryable)]
pub struct DocumentType {
    pub document_type_id: i32,
    pub name: String,
}

#[derive(Serialize, Deserialize, Debug, Queryable)]
pub struct Document {
    pub document_id: i32,
    pub version_major: i32,
    pub version_minor: i32,
    pub content: String,
    pub created_at: NaiveDateTime
}

#[derive(Insertable, Deserialize, Clone, Debug)]
#[table_name="document"]
pub struct NewDocument {
    pub version_major: i32,
    pub version_minor: i32,
    pub content: String
}
