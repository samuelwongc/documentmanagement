use std::time::SystemTime;

enum DocumentType {
    TermsAndConditions,
    Contract
}

struct User {
    lender: String,
    email: String,
    sign_in_count: u64,
    active: bool,
}

struct Document {
    created_at: SystemTime,
    created_by: User,
    type: DocumentType,
    content: String
}
