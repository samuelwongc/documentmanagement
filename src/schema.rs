table! {
    document (document_id) {
        document_id -> Int4,
        version_major -> Int4,
        version_minor -> Int4,
        content -> Text,
        created_at -> Timestamp,
    }
}

table! {
    document_type (document_type_id) {
        document_type_id -> Int4,
        name -> Varchar,
    }
}

allow_tables_to_appear_in_same_query!(
    document,
    document_type,
);
