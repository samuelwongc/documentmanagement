#[macro_use] extern crate diesel;
#[macro_use] extern crate serde_derive;
extern crate bodyparser;
extern crate iron;
extern crate time;
extern crate router;
extern crate dotenv;
extern crate serde_json;
extern crate chrono;

use diesel::prelude::*;
use diesel::pg::PgConnection;
use dotenv::dotenv;
use iron::prelude::*;
use iron::{BeforeMiddleware, AfterMiddleware, typemap};
use iron::mime::Mime;
use iron::status;
use router::Router;
use time::precise_time_ns;
use std::env;

mod models;
mod schema;

pub fn establish_connection() -> PgConnection {
    dotenv().ok();

    let database_url = env::var("DATABASE_URL")
        .expect("DATABASE_URL must be set");
    PgConnection::establish(&database_url)
        .expect(&format!("Error connecting to {}", database_url))
}

struct ResponseTime;

impl typemap::Key for ResponseTime { type Value = u64; }

impl BeforeMiddleware for ResponseTime {
    fn before(&self, req: &mut Request) -> IronResult<()> {
        req.extensions.insert::<ResponseTime>(precise_time_ns());
        Ok(())
    }
}

impl AfterMiddleware for ResponseTime {
    fn after(&self, req: &mut Request, res: Response) -> IronResult<Response> {
        let delta = precise_time_ns() - *req.extensions.get::<ResponseTime>().unwrap();
        println!("Request took: {} ms", (delta as f64) / 1000000.0);
        Ok(res)
    }
}

fn hello_world(_: &mut Request) -> IronResult<Response> {
    Ok(Response::with((status::Ok, "Hello World")))
}

fn document_list(_req: &mut Request) -> IronResult<Response> {
    use schema::document::dsl::*;
    let results = document
        .load::<models::Document>(&establish_connection())
        .expect("Error");
    let content_type = "application/json".parse::<Mime>().unwrap();
    let documents = results.iter().map(|r| serde_json::to_string(&r).unwrap()).fold("".to_string(), |x, y| format!("{},{}", x, y));
    Ok(Response::with((content_type, status::Ok, format!("{{ documents:{} }}", documents))))
}


fn document_create(req: &mut Request) -> IronResult<Response> {
    use schema::document;

    let result : Option<models::Document> = match req.get::<bodyparser::Struct<models::NewDocument>>() {
        Ok(Some(new_document)) => Some(diesel::insert_into(document::table)
                                        .values(&new_document)
                                        .get_result::<models::Document>(&establish_connection())
                                        .expect("Error saving new document")),
        _ => None,
    };

    let content_type = "application/json".parse::<Mime>().unwrap();
    match result {
        Some(result) => Ok(Response::with((content_type, status::Ok, serde_json::to_string(&result).unwrap()))),
        _ => Ok(Response::with((content_type, status::BadRequest, "Shit happened".to_string()))),
    }
}

/*
fn document_retrieve(req: &mut Request) -> IronResult<Response> {
    let ref document_id = req.extensions.get::<Router>().unwrap().find("document_id").unwrap_or("/");
    Ok(Response::with((status::Ok, *documentId)))
}

fn document_update(req: &mut Request) -> IronResult<Response> {
    let ref document_id = req.extensions.get::<Router>().unwrap().find("document_id").unwrap_or("/");
    Ok(Response::with((status::Ok, *documentId)))
}
*/

fn get_router() -> Router {
    let mut router = Router::new();
    router.get("/", hello_world, "index");
    router.get("/document", document_list, "document_list");
    router.post("/document", document_create, "document_create");
    //router.get("/document/:documentId/", document_retrieve, "document_retrieve");
    //router.patch("/document/:documentId/", document_update, "document_update");
    router
}

fn main() {
    Iron::new(get_router()).http("localhost:3000").unwrap();
}
