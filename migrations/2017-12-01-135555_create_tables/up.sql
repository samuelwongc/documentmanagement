CREATE TABLE document_type (
    document_type_id SERIAL PRIMARY KEY,
    name VARCHAR NOT NULL
);

CREATE TABLE document (
    document_id SERIAL PRIMARY KEY,
    version_major INTEGER NOT NULL,
    version_minor INTEGER NOT NULL,
    content TEXT NOT NULL,
    created_at TIMESTAMP NOT NULL
);